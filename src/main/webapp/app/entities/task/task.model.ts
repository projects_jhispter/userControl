import { BaseEntity, User } from './../../shared';

export class Task implements BaseEntity {
    constructor(
        public id?: number,
        public taskDescription?: string,
        public last?: number,
        public date?: any,
        public user?: User,
        public tasktype?: BaseEntity,
        public area?: BaseEntity,
        public project?: BaseEntity,
    ) {
    }
}
