import { BaseEntity } from './../../shared';

export class Area implements BaseEntity {
    constructor(
        public id?: number,
        public areaName?: string,
        public description?: string,
    ) {
    }
}
