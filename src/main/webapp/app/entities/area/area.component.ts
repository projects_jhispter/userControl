import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Area } from './area.model';
import { AreaService } from './area.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-area',
    templateUrl: './area.component.html'
})
export class AreaComponent implements OnInit, OnDestroy {
areas: Area[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private areaService: AreaService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.areaService.query().subscribe(
            (res: HttpResponse<Area[]>) => {
                this.areas = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAreas();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Area) {
        return item.id;
    }
    registerChangeInAreas() {
        this.eventSubscriber = this.eventManager.subscribe('areaListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
