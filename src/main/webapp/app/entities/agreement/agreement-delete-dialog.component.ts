import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Agreement } from './agreement.model';
import { AgreementPopupService } from './agreement-popup.service';
import { AgreementService } from './agreement.service';

@Component({
    selector: 'jhi-agreement-delete-dialog',
    templateUrl: './agreement-delete-dialog.component.html'
})
export class AgreementDeleteDialogComponent {

    agreement: Agreement;

    constructor(
        private agreementService: AgreementService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.agreementService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'agreementListModification',
                content: 'Deleted an agreement'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-agreement-delete-popup',
    template: ''
})
export class AgreementDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private agreementPopupService: AgreementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.agreementPopupService
                .open(AgreementDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
