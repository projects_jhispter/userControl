import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Agreement } from './agreement.model';
import { AgreementPopupService } from './agreement-popup.service';
import { AgreementService } from './agreement.service';
import { Project, ProjectService } from '../project';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-agreement-dialog',
    templateUrl: './agreement-dialog.component.html'
})
export class AgreementDialogComponent implements OnInit {

    agreement: Agreement;
    isSaving: boolean;

    projects: Project[];

    users: User[];
    dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private agreementService: AgreementService,
        private projectService: ProjectService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.projectService.query()
            .subscribe((res: HttpResponse<Project[]>) => { this.projects = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.agreement.id !== undefined) {
            this.subscribeToSaveResponse(
                this.agreementService.update(this.agreement));
        } else {
            this.subscribeToSaveResponse(
                this.agreementService.create(this.agreement));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Agreement>>) {
        result.subscribe((res: HttpResponse<Agreement>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Agreement) {
        this.eventManager.broadcast({ name: 'agreementListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackProjectById(index: number, item: Project) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-agreement-popup',
    template: ''
})
export class AgreementPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private agreementPopupService: AgreementPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.agreementPopupService
                    .open(AgreementDialogComponent as Component, params['id']);
            } else {
                this.agreementPopupService
                    .open(AgreementDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
