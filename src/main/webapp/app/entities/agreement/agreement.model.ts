import { BaseEntity, User } from './../../shared';

export class Agreement implements BaseEntity {
    constructor(
        public id?: number,
        public agreementName?: string,
        public description?: string,
        public date?: any,
        public project?: BaseEntity,
        public user?: User,
    ) {
    }
}
