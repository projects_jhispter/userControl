import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { UserControlSharedModule } from '../../shared';
import { UserControlAdminModule } from '../../admin/admin.module';
import {
    AgreementService,
    AgreementPopupService,
    AgreementComponent,
    AgreementDetailComponent,
    AgreementDialogComponent,
    AgreementPopupComponent,
    AgreementDeletePopupComponent,
    AgreementDeleteDialogComponent,
    agreementRoute,
    agreementPopupRoute,
} from './';

const ENTITY_STATES = [
    ...agreementRoute,
    ...agreementPopupRoute,
];

@NgModule({
    imports: [
        UserControlSharedModule,
        UserControlAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AgreementComponent,
        AgreementDetailComponent,
        AgreementDialogComponent,
        AgreementDeleteDialogComponent,
        AgreementPopupComponent,
        AgreementDeletePopupComponent,
    ],
    entryComponents: [
        AgreementComponent,
        AgreementDialogComponent,
        AgreementPopupComponent,
        AgreementDeleteDialogComponent,
        AgreementDeletePopupComponent,
    ],
    providers: [
        AgreementService,
        AgreementPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserControlAgreementModule {}
