import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Agreement } from './agreement.model';
import { AgreementService } from './agreement.service';

@Component({
    selector: 'jhi-agreement-detail',
    templateUrl: './agreement-detail.component.html'
})
export class AgreementDetailComponent implements OnInit, OnDestroy {

    agreement: Agreement;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private agreementService: AgreementService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAgreements();
    }

    load(id) {
        this.agreementService.find(id)
            .subscribe((agreementResponse: HttpResponse<Agreement>) => {
                this.agreement = agreementResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAgreements() {
        this.eventSubscriber = this.eventManager.subscribe(
            'agreementListModification',
            (response) => this.load(this.agreement.id)
        );
    }
}
