import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AgreementComponent } from './agreement.component';
import { AgreementDetailComponent } from './agreement-detail.component';
import { AgreementPopupComponent } from './agreement-dialog.component';
import { AgreementDeletePopupComponent } from './agreement-delete-dialog.component';

export const agreementRoute: Routes = [
    {
        path: 'agreement',
        component: AgreementComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.agreement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'agreement/:id',
        component: AgreementDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.agreement.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const agreementPopupRoute: Routes = [
    {
        path: 'agreement-new',
        component: AgreementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.agreement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'agreement/:id/edit',
        component: AgreementPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.agreement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'agreement/:id/delete',
        component: AgreementDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.agreement.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
