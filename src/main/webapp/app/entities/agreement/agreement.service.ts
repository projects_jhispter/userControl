import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Agreement } from './agreement.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Agreement>;

@Injectable()
export class AgreementService {

    private resourceUrl =  SERVER_API_URL + 'api/agreements';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(agreement: Agreement): Observable<EntityResponseType> {
        const copy = this.convert(agreement);
        return this.http.post<Agreement>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(agreement: Agreement): Observable<EntityResponseType> {
        const copy = this.convert(agreement);
        return this.http.put<Agreement>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Agreement>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Agreement[]>> {
        const options = createRequestOption(req);
        return this.http.get<Agreement[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Agreement[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Agreement = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Agreement[]>): HttpResponse<Agreement[]> {
        const jsonResponse: Agreement[] = res.body;
        const body: Agreement[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Agreement.
     */
    private convertItemFromServer(agreement: Agreement): Agreement {
        const copy: Agreement = Object.assign({}, agreement);
        copy.date = this.dateUtils
            .convertLocalDateFromServer(agreement.date);
        return copy;
    }

    /**
     * Convert a Agreement to a JSON which can be sent to the server.
     */
    private convert(agreement: Agreement): Agreement {
        const copy: Agreement = Object.assign({}, agreement);
        copy.date = this.dateUtils
            .convertLocalDateToServer(agreement.date);
        return copy;
    }
}
