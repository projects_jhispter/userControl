import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { Agreement } from './agreement.model';
import { AgreementService } from './agreement.service';

@Injectable()
export class AgreementPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private agreementService: AgreementService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.agreementService.find(id)
                    .subscribe((agreementResponse: HttpResponse<Agreement>) => {
                        const agreement: Agreement = agreementResponse.body;
                        if (agreement.date) {
                            agreement.date = {
                                year: agreement.date.getFullYear(),
                                month: agreement.date.getMonth() + 1,
                                day: agreement.date.getDate()
                            };
                        }
                        this.ngbModalRef = this.agreementModalRef(component, agreement);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.agreementModalRef(component, new Agreement());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    agreementModalRef(component: Component, agreement: Agreement): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.agreement = agreement;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
