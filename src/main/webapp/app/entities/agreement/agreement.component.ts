import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Agreement } from './agreement.model';
import { AgreementService } from './agreement.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-agreement',
    templateUrl: './agreement.component.html'
})
export class AgreementComponent implements OnInit, OnDestroy {
agreements: Agreement[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private agreementService: AgreementService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.agreementService.query().subscribe(
            (res: HttpResponse<Agreement[]>) => {
                this.agreements = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAgreements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Agreement) {
        return item.id;
    }
    registerChangeInAgreements() {
        this.eventSubscriber = this.eventManager.subscribe('agreementListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
