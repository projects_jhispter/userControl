import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { UserControlProjectModule } from './project/project.module';
import { UserControlTaskModule } from './task/task.module';
import { UserControlCommentsModule } from './comments/comments.module';
import { UserControlTaskTypeModule } from './task-type/task-type.module';
import { UserControlAreaModule } from './area/area.module';
import { UserControlAgreementModule } from './agreement/agreement.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        UserControlProjectModule,
        UserControlTaskModule,
        UserControlCommentsModule,
        UserControlTaskTypeModule,
        UserControlAreaModule,
        UserControlAgreementModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserControlEntityModule {}
