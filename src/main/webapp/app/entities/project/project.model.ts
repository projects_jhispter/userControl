import { BaseEntity, User } from './../../shared';

export class Project implements BaseEntity {
    constructor(
        public id?: number,
        public projectName?: string,
        public projectDescription?: string,
        public initialDate?: any,
        public endDate?: any,
        public status?: boolean,
        public users?: User[],
        public clients?: User[],
        public teamleads?: User[],
    ) {
        this.status = false;
    }
}
