import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { UserControlSharedModule } from '../../shared';
import {
    TaskTypeService,
    TaskTypePopupService,
    TaskTypeComponent,
    TaskTypeDetailComponent,
    TaskTypeDialogComponent,
    TaskTypePopupComponent,
    TaskTypeDeletePopupComponent,
    TaskTypeDeleteDialogComponent,
    taskTypeRoute,
    taskTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...taskTypeRoute,
    ...taskTypePopupRoute,
];

@NgModule({
    imports: [
        UserControlSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TaskTypeComponent,
        TaskTypeDetailComponent,
        TaskTypeDialogComponent,
        TaskTypeDeleteDialogComponent,
        TaskTypePopupComponent,
        TaskTypeDeletePopupComponent,
    ],
    entryComponents: [
        TaskTypeComponent,
        TaskTypeDialogComponent,
        TaskTypePopupComponent,
        TaskTypeDeleteDialogComponent,
        TaskTypeDeletePopupComponent,
    ],
    providers: [
        TaskTypeService,
        TaskTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UserControlTaskTypeModule {}
