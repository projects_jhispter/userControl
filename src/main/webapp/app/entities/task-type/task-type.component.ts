import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TaskType } from './task-type.model';
import { TaskTypeService } from './task-type.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-task-type',
    templateUrl: './task-type.component.html'
})
export class TaskTypeComponent implements OnInit, OnDestroy {
taskTypes: TaskType[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private taskTypeService: TaskTypeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.taskTypeService.query().subscribe(
            (res: HttpResponse<TaskType[]>) => {
                this.taskTypes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTaskTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TaskType) {
        return item.id;
    }
    registerChangeInTaskTypes() {
        this.eventSubscriber = this.eventManager.subscribe('taskTypeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
