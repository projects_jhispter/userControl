import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TaskTypeComponent } from './task-type.component';
import { TaskTypeDetailComponent } from './task-type-detail.component';
import { TaskTypePopupComponent } from './task-type-dialog.component';
import { TaskTypeDeletePopupComponent } from './task-type-delete-dialog.component';

export const taskTypeRoute: Routes = [
    {
        path: 'task-type',
        component: TaskTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.taskType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'task-type/:id',
        component: TaskTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.taskType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const taskTypePopupRoute: Routes = [
    {
        path: 'task-type-new',
        component: TaskTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.taskType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'task-type/:id/edit',
        component: TaskTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.taskType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'task-type/:id/delete',
        component: TaskTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'userControlApp.taskType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
