import { BaseEntity } from './../../shared';

export class TaskType implements BaseEntity {
    constructor(
        public id?: number,
        public taskType?: string,
        public description?: string,
        public archive?: boolean,
    ) {
        this.archive = false;
    }
}
