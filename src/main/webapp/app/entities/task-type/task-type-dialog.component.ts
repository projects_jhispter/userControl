import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TaskType } from './task-type.model';
import { TaskTypePopupService } from './task-type-popup.service';
import { TaskTypeService } from './task-type.service';

@Component({
    selector: 'jhi-task-type-dialog',
    templateUrl: './task-type-dialog.component.html'
})
export class TaskTypeDialogComponent implements OnInit {

    taskType: TaskType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private taskTypeService: TaskTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.taskType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.taskTypeService.update(this.taskType));
        } else {
            this.subscribeToSaveResponse(
                this.taskTypeService.create(this.taskType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TaskType>>) {
        result.subscribe((res: HttpResponse<TaskType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TaskType) {
        this.eventManager.broadcast({ name: 'taskTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-task-type-popup',
    template: ''
})
export class TaskTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taskTypePopupService: TaskTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.taskTypePopupService
                    .open(TaskTypeDialogComponent as Component, params['id']);
            } else {
                this.taskTypePopupService
                    .open(TaskTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
