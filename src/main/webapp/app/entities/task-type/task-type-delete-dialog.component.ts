import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TaskType } from './task-type.model';
import { TaskTypePopupService } from './task-type-popup.service';
import { TaskTypeService } from './task-type.service';

@Component({
    selector: 'jhi-task-type-delete-dialog',
    templateUrl: './task-type-delete-dialog.component.html'
})
export class TaskTypeDeleteDialogComponent {

    taskType: TaskType;

    constructor(
        private taskTypeService: TaskTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.taskTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'taskTypeListModification',
                content: 'Deleted an taskType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-task-type-delete-popup',
    template: ''
})
export class TaskTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private taskTypePopupService: TaskTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.taskTypePopupService
                .open(TaskTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
