import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TaskType } from './task-type.model';
import { TaskTypeService } from './task-type.service';

@Component({
    selector: 'jhi-task-type-detail',
    templateUrl: './task-type-detail.component.html'
})
export class TaskTypeDetailComponent implements OnInit, OnDestroy {

    taskType: TaskType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private taskTypeService: TaskTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTaskTypes();
    }

    load(id) {
        this.taskTypeService.find(id)
            .subscribe((taskTypeResponse: HttpResponse<TaskType>) => {
                this.taskType = taskTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTaskTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'taskTypeListModification',
            (response) => this.load(this.taskType.id)
        );
    }
}
