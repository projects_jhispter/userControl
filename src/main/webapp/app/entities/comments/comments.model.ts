import { BaseEntity, User } from './../../shared';

export class Comments implements BaseEntity {
    constructor(
        public id?: number,
        public commentDescriptions?: string,
        public dateCreated?: any,
        public user?: User,
        public task?: BaseEntity,
    ) {
    }
}
