/**
 * View Models used by Spring MVC REST controllers.
 */
package us.control.web.rest.vm;
