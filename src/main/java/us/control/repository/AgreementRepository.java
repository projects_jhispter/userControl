package us.control.repository;

import us.control.domain.Agreement;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Agreement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgreementRepository extends JpaRepository<Agreement, Long> {

    @Query("select agreement from Agreement agreement where agreement.user.login = ?#{principal.username}")
    List<Agreement> findByUserIsCurrentUser();

}
