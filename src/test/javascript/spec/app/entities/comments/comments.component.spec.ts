/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { UserControlTestModule } from '../../../test.module';
import { CommentsComponent } from '../../../../../../main/webapp/app/entities/comments/comments.component';
import { CommentsService } from '../../../../../../main/webapp/app/entities/comments/comments.service';
import { Comments } from '../../../../../../main/webapp/app/entities/comments/comments.model';

describe('Component Tests', () => {

    describe('Comments Management Component', () => {
        let comp: CommentsComponent;
        let fixture: ComponentFixture<CommentsComponent>;
        let service: CommentsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [UserControlTestModule],
                declarations: [CommentsComponent],
                providers: [
                    CommentsService
                ]
            })
            .overrideTemplate(CommentsComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CommentsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CommentsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Comments(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.comments[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
