/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { UserControlTestModule } from '../../../test.module';
import { CommentsDetailComponent } from '../../../../../../main/webapp/app/entities/comments/comments-detail.component';
import { CommentsService } from '../../../../../../main/webapp/app/entities/comments/comments.service';
import { Comments } from '../../../../../../main/webapp/app/entities/comments/comments.model';

describe('Component Tests', () => {

    describe('Comments Management Detail Component', () => {
        let comp: CommentsDetailComponent;
        let fixture: ComponentFixture<CommentsDetailComponent>;
        let service: CommentsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [UserControlTestModule],
                declarations: [CommentsDetailComponent],
                providers: [
                    CommentsService
                ]
            })
            .overrideTemplate(CommentsDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CommentsDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CommentsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Comments(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.comments).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
