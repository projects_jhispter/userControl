/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { UserControlTestModule } from '../../../test.module';
import { TaskTypeDetailComponent } from '../../../../../../main/webapp/app/entities/task-type/task-type-detail.component';
import { TaskTypeService } from '../../../../../../main/webapp/app/entities/task-type/task-type.service';
import { TaskType } from '../../../../../../main/webapp/app/entities/task-type/task-type.model';

describe('Component Tests', () => {

    describe('TaskType Management Detail Component', () => {
        let comp: TaskTypeDetailComponent;
        let fixture: ComponentFixture<TaskTypeDetailComponent>;
        let service: TaskTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [UserControlTestModule],
                declarations: [TaskTypeDetailComponent],
                providers: [
                    TaskTypeService
                ]
            })
            .overrideTemplate(TaskTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaskTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaskTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TaskType(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.taskType).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
