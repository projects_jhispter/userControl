/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { UserControlTestModule } from '../../../test.module';
import { TaskTypeComponent } from '../../../../../../main/webapp/app/entities/task-type/task-type.component';
import { TaskTypeService } from '../../../../../../main/webapp/app/entities/task-type/task-type.service';
import { TaskType } from '../../../../../../main/webapp/app/entities/task-type/task-type.model';

describe('Component Tests', () => {

    describe('TaskType Management Component', () => {
        let comp: TaskTypeComponent;
        let fixture: ComponentFixture<TaskTypeComponent>;
        let service: TaskTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [UserControlTestModule],
                declarations: [TaskTypeComponent],
                providers: [
                    TaskTypeService
                ]
            })
            .overrideTemplate(TaskTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TaskTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TaskTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TaskType(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.taskTypes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
