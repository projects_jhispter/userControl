/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { UserControlTestModule } from '../../../test.module';
import { AgreementDetailComponent } from '../../../../../../main/webapp/app/entities/agreement/agreement-detail.component';
import { AgreementService } from '../../../../../../main/webapp/app/entities/agreement/agreement.service';
import { Agreement } from '../../../../../../main/webapp/app/entities/agreement/agreement.model';

describe('Component Tests', () => {

    describe('Agreement Management Detail Component', () => {
        let comp: AgreementDetailComponent;
        let fixture: ComponentFixture<AgreementDetailComponent>;
        let service: AgreementService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [UserControlTestModule],
                declarations: [AgreementDetailComponent],
                providers: [
                    AgreementService
                ]
            })
            .overrideTemplate(AgreementDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AgreementDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgreementService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Agreement(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.agreement).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
