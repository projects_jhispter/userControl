/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { UserControlTestModule } from '../../../test.module';
import { AgreementComponent } from '../../../../../../main/webapp/app/entities/agreement/agreement.component';
import { AgreementService } from '../../../../../../main/webapp/app/entities/agreement/agreement.service';
import { Agreement } from '../../../../../../main/webapp/app/entities/agreement/agreement.model';

describe('Component Tests', () => {

    describe('Agreement Management Component', () => {
        let comp: AgreementComponent;
        let fixture: ComponentFixture<AgreementComponent>;
        let service: AgreementService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [UserControlTestModule],
                declarations: [AgreementComponent],
                providers: [
                    AgreementService
                ]
            })
            .overrideTemplate(AgreementComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AgreementComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgreementService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Agreement(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.agreements[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
